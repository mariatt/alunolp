program Project3;

uses
  Vcl.Forms,
  Unit4 in 'Unit4.pas' {Form4},
  Unit5 in 'Unit5.pas' {DataModule5: TDataModule},
  pokemon in 'pokemon.pas' {Form6};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TDataModule5, DataModule5);
  Application.CreateForm(TForm6, Form6);
  Application.Run;
end.
